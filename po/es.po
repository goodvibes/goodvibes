# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the goodvibes package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: goodvibes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-28 16:52+0700\n"
"PO-Revision-Date: 2024-04-29 12:07+0000\n"
"Last-Translator: gallegonovato <fran-carro@hotmail.es>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/goodvibes/"
"translations/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.5.2\n"

#: data/io.gitlab.Goodvibes.appdata.xml.in:4
#: data/io.gitlab.Goodvibes.desktop.in:3 src/main.c:129
msgid "Goodvibes"
msgstr "Goodvibes"

#: data/io.gitlab.Goodvibes.appdata.xml.in:5
#: data/io.gitlab.Goodvibes.desktop.in:5
msgid "Play web radios"
msgstr "Escuchar radios online"

#: data/io.gitlab.Goodvibes.appdata.xml.in:8
msgid "Arnaud Rebillout"
msgstr "Arnaud Rebillout"

#: data/io.gitlab.Goodvibes.appdata.xml.in:12
msgid "Goodvibes is a simple internet radio player for GNU/Linux."
msgstr ""
"Goodvibes es un simple reproductor de radios de internet para GNU/Linux."

#: data/io.gitlab.Goodvibes.appdata.xml.in:15
msgid ""
"It comes with every basic features you can expect from an audio player, such "
"as multimedia keys, notifications, system sleep inhibition, and MPRIS2 "
"support."
msgstr ""
"Viene con todas las funciones básicas que puede esperar de un reproductor de "
"audio, tales como teclas multimedia, notificaciones, supresión de la "
"suspensión del sistema y compatibilidad con MPRIS2."

#: data/io.gitlab.Goodvibes.desktop.in:4
msgid "Radio Player"
msgstr "Reproductor de radio"

#. TRANSLATORS: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/io.gitlab.Goodvibes.desktop.in:7
msgid "Audio;Radio;Player;"
msgstr "Audio;Radio;Reproductor;"

#: src/ui/resources/app-menu.ui:6 src/ui/resources/prefs-window.glade:563
#: src/ui/resources/shortcuts-window.ui:16
#: src/ui/resources/status-icon-menu.ui:6
msgid "Play/Stop"
msgstr "Reproducir/detener"

#: src/ui/resources/app-menu.ui:10 src/ui/resources/shortcuts-window.ui:30
#: src/ui/resources/status-icon-menu.ui:10 src/ui/gv-station-context-menu.c:34
#: src/ui/gv-station-dialog.c:500
msgid "Add Station"
msgstr "Añadir emisora"

#: src/ui/resources/app-menu.ui:16 src/ui/resources/status-icon-menu.ui:16
#: src/ui/gv-prefs-window.c:599
msgid "Preferences"
msgstr "Preferencias"

#: src/ui/resources/app-menu.ui:22
msgid "Keyboard Shortcuts"
msgstr "Atajos del teclado"

#: src/ui/resources/app-menu.ui:26 src/ui/resources/shortcuts-window.ui:37
#: src/ui/resources/status-icon-menu.ui:22
msgid "Online Help"
msgstr "Ayuda Online"

#: src/ui/resources/app-menu.ui:30 src/ui/resources/status-icon-menu.ui:26
msgid "About"
msgstr "Acerca de"

#: src/ui/resources/app-menu.ui:34 src/ui/resources/prefs-window.glade:42
#: src/ui/resources/shortcuts-window.ui:44
msgid "Close"
msgstr "Cerrar"

#: src/ui/resources/app-menu.ui:38 src/ui/resources/prefs-window.glade:41
#: src/ui/resources/shortcuts-window.ui:51
#: src/ui/resources/status-icon-menu.ui:30
msgid "Quit"
msgstr "Salir"

#: src/ui/resources/playlist-view.glade:49
#: src/ui/resources/station-view.glade:25 src/ui/gv-playlist-view.c:109
#: src/ui/gv-station-view.c:362
msgid "No station selected"
msgstr "No se seleccionó ninguna emisora"

#: src/ui/resources/playlist-view.glade:65
#: src/ui/resources/station-view.glade:41
msgid "Stopped"
msgstr "Detenido"

#: src/ui/resources/prefs-window.glade:55
msgid "Close Button"
msgstr "Botón Cerrar"

#: src/ui/resources/prefs-window.glade:68
msgid "Application"
msgstr "Aplicación"

#: src/ui/resources/prefs-window.glade:91
msgid "Autoplay on Startup"
msgstr "Reproducir automáticamente al iniciar"

#: src/ui/resources/prefs-window.glade:104
msgid "Custom Output Pipeline"
msgstr "Canalización de salida personalizada"

#: src/ui/resources/prefs-window.glade:135
msgid "Apply"
msgstr "Aplicar"

#: src/ui/resources/prefs-window.glade:158
msgid "Playback"
msgstr "Reproducción"

#: src/ui/resources/prefs-window.glade:195
msgid "Prevent sleep while playing"
msgstr "Evitar suspensión mientras se reproduce"

#: src/ui/resources/prefs-window.glade:208
msgid "System"
msgstr "Sistema"

#: src/ui/resources/prefs-window.glade:255
msgid "Native D-Bus Server"
msgstr "Servidor D-Bus nativo"

#: src/ui/resources/prefs-window.glade:267
msgid "MPRIS2 D-Bus Server"
msgstr "Servidor MPRIS2 D-Bus"

#: src/ui/resources/prefs-window.glade:280
msgid "D-Bus"
msgstr "D-Bus"

#: src/ui/resources/prefs-window.glade:296
#: src/ui/resources/shortcuts-window.ui:11
msgid "General"
msgstr "Generales"

#: src/ui/resources/prefs-window.glade:324
msgid "Theme Variant"
msgstr "Variante de tema"

#: src/ui/resources/prefs-window.glade:337
msgid "System Default"
msgstr "Predeterminada del sistema"

#: src/ui/resources/prefs-window.glade:338
msgid "Prefer Dark"
msgstr "Preferir oscura"

#: src/ui/resources/prefs-window.glade:339
msgid "Prefer Light"
msgstr "Preferir clara"

#: src/ui/resources/prefs-window.glade:353
msgid "Window"
msgstr "Ventana"

#: src/ui/resources/prefs-window.glade:380
msgid "Send Notifications"
msgstr "Enviar notificaciones"

#: src/ui/resources/prefs-window.glade:403
msgid "Notifications"
msgstr "Notificaciones"

#: src/ui/resources/prefs-window.glade:439
msgid "Console Output"
msgstr "Salida de consola"

#: src/ui/resources/prefs-window.glade:452
msgid "Console"
msgstr "Consola"

#: src/ui/resources/prefs-window.glade:471
msgid "Display"
msgstr "Interfaz"

#: src/ui/resources/prefs-window.glade:510
msgid "Multimedia Hotkeys"
msgstr "Teclas multimedia"

#: src/ui/resources/prefs-window.glade:523
msgid "Keyboard"
msgstr "Teclado"

#: src/ui/resources/prefs-window.glade:550
msgid "Middle Click"
msgstr "Pulsación central"

#: src/ui/resources/prefs-window.glade:564
#: src/ui/resources/shortcuts-window.ui:23
msgid "Mute"
msgstr "Silenciar"

#: src/ui/resources/prefs-window.glade:577
msgid "Scrolling"
msgstr "Desplazamiento"

#: src/ui/resources/prefs-window.glade:590
msgid "Change Station"
msgstr "Cambiar de emisora"

#: src/ui/resources/prefs-window.glade:591
msgid "Change Volume"
msgstr "Cambiar volumen"

#: src/ui/resources/prefs-window.glade:605
msgid "Mouse (Status Icon Mode)"
msgstr "Ratón (modo icono de estado)"

#: src/ui/resources/prefs-window.glade:624
msgid "Controls"
msgstr "Controles"

#: src/ui/resources/station-dialog.glade:13
msgid "Name"
msgstr "Nombre"

#: src/ui/resources/station-dialog.glade:36
#: src/ui/resources/station-view.glade:447
msgid "URL"
msgstr "URL"

#: src/ui/resources/station-dialog.glade:63
msgid "Security Exception"
msgstr "Excepción de seguridad"

#: src/ui/resources/station-dialog.glade:74 src/ui/gv-station-context-menu.c:96
msgid "Remove"
msgstr "Quitar"

#: src/ui/resources/station-view.glade:89
msgid "Station"
msgstr "Emisora"

#: src/ui/resources/station-view.glade:103
msgid "Comment"
msgstr "Comentario"

#: src/ui/resources/station-view.glade:114
msgid "Year"
msgstr "Año"

#: src/ui/resources/station-view.glade:125
msgid "Genre"
msgstr "Género"

#: src/ui/resources/station-view.glade:136
msgid "Album"
msgstr "Álbum"

#: src/ui/resources/station-view.glade:147
msgid "Artist"
msgstr "Artista"

#: src/ui/resources/station-view.glade:158
msgid "Title"
msgstr "Título"

#: src/ui/resources/station-view.glade:247
msgid "Metadata"
msgstr "Metadatos"

#: src/ui/resources/station-view.glade:261 src/feat/gv-notifications.c:139
msgid "Error"
msgstr "Error"

#: src/ui/resources/station-view.glade:275
msgid "Message"
msgstr "Mensaje"

#: src/ui/resources/station-view.glade:314
msgid "Details"
msgstr "Detalles"

#: src/ui/resources/station-view.glade:325
msgid "Bitrate"
msgstr "Tasa de bits"

#: src/ui/resources/station-view.glade:336
msgid "Sample Rate"
msgstr "Tasa de muestreo"

#: src/ui/resources/station-view.glade:347
msgid "Channels"
msgstr "Canales"

#: src/ui/resources/station-view.glade:358
msgid "Codec"
msgstr "Códec"

#: src/ui/resources/station-view.glade:369
msgid "Stream Type"
msgstr "Tipo de corriente"

#: src/ui/resources/station-view.glade:380
msgid "User-Agent"
msgstr "Agente de usuario"

#: src/ui/resources/station-view.glade:391
#: src/ui/resources/station-view.glade:425 src/ui/gv-certificate-dialog.c:336
#: src/ui/gv-certificate-dialog.c:338
msgid "Redirection"
msgstr "Redirección"

#: src/ui/resources/station-view.glade:402 src/ui/gv-certificate-dialog.c:337
msgid "Stream URL"
msgstr "URL de la transmisión"

#: src/ui/resources/station-view.glade:414
msgid "Streams"
msgstr "Transmisiones"

#: src/ui/resources/station-view.glade:436 src/ui/gv-certificate-dialog.c:335
msgid "Playlist URL"
msgstr "URL de la lista de reproducción"

#: src/core/gv-engine.c:259
msgid "Failed to parse pipeline description"
msgstr "No se pudo analizar la descripción de la canalización de salida"

#: src/core/gv-station-list.c:1316
msgid "Failed to save station list"
msgstr "No se pudo guardar la lista de emisoras"

#: src/ui/gv-certificate-dialog.c:339
msgid "TLS Errors"
msgstr "Errores TLS"

#: src/ui/gv-certificate-dialog.c:355
msgid "Add a Security Exception?"
msgstr "¿Añadir una excepción de seguridad?"

#: src/ui/gv-certificate-dialog.c:356
msgid ""
"The TLS certificate for this station is not valid. The issue is most likely "
"a misconfiguration of the website."
msgstr ""
"El certificado TLS para esta emisora no es válido. Lo más probable es que se "
"trate de un error de configuración de la página web."

#: src/ui/gv-certificate-dialog.c:363 src/ui/gv-station-context-menu.c:95
#: src/ui/gv-station-dialog.c:353
msgid "Cancel"
msgstr "Cancelar"

#: src/ui/gv-certificate-dialog.c:364
msgid "Continue"
msgstr "Continuar"

#: src/ui/gv-prefs-window.c:259
msgid "Feature disabled at compile-time."
msgstr "Función desactivada durante la compilación."

#: src/ui/gv-prefs-window.c:401
msgid "Action when the close button is clicked."
msgstr "Acción al pulsar en el botón Cerrar."

#: src/ui/gv-prefs-window.c:407
msgid "Setting not available in status icon mode."
msgstr "Configuración no disponible en el modo de icono de estado."

#: src/ui/gv-prefs-window.c:411
msgid "Whether to start playback automatically on startup."
msgstr "Indica si se iniciará la reproducción automáticamente al iniciar."

#: src/ui/gv-prefs-window.c:417
msgid "Whether to use a custom output pipeline."
msgstr "Indica si se utilizará una canalización de salida personalizada."

#: src/ui/gv-prefs-window.c:423
msgid ""
"The GStreamer output pipeline used for playback. Refer to the online "
"documentation for examples."
msgstr ""
"La canalización de salida de GStreamer utilizada para la reproducción. "
"Consulte la documentación en línea para obtener ejemplos."

#: src/ui/gv-prefs-window.c:439
msgid "Prevent the system from going to sleep while playing."
msgstr "Evitar que el sistema se suspenda durante la reproducción."

#: src/ui/gv-prefs-window.c:444
msgid "Enable the native D-Bus server (needed for the command-line interface)."
msgstr ""
"Activar el servidor D-Bus nativo (necesario para la interfaz de línea de "
"órdenes)."

#: src/ui/gv-prefs-window.c:450
msgid "Enable the MPRIS2 D-Bus server."
msgstr "Activar el servidor MPRIS2 de D-Bus."

#. Display
#: src/ui/gv-prefs-window.c:456
msgid "Prefer a different variant of the theme (if available)."
msgstr "Preferir una variante diferente del tema (si se encuentra disponible)."

#: src/ui/gv-prefs-window.c:462
msgid "Show notification when the status changes."
msgstr "Mostrar una notificación cuando cambie el estado."

#: src/ui/gv-prefs-window.c:467
msgid "Display information on the standard output."
msgstr "Mostrar información en la salida estándar."

#. Controls
#: src/ui/gv-prefs-window.c:473
msgid "Bind mutimedia keys (play/pause/stop/previous/next)."
msgstr ""
"Asignar teclas multimedia (reproducir/pausa/detener/anterior/siguiente)."

#: src/ui/gv-prefs-window.c:479
msgid "Action triggered by a middle click on the status icon."
msgstr ""
"Acción provocada por una pulsación con el botón central del ratón en el "
"icono de estado."

#: src/ui/gv-prefs-window.c:485
msgid "Action triggered by mouse-scrolling on the status icon."
msgstr ""
"Acción provocada por el movimiento de la rueda del ratón en el icono de "
"estado."

#: src/ui/gv-prefs-window.c:491
msgid "Setting only available in status icon mode."
msgstr "Configuración solo disponible en el modo de icono de estado."

#. We might be creating a new station, or editing an existing one
#: src/ui/gv-station-context-menu.c:35 src/ui/gv-station-dialog.c:500
msgid "Edit Station"
msgstr "Editar emisora"

#: src/ui/gv-station-context-menu.c:36
msgid "Remove Station"
msgstr "Quitar emisora"

#: src/ui/gv-station-context-menu.c:37
msgid "Remove all Stations"
msgstr "Eliminar todas las estaciones"

#: src/ui/gv-station-context-menu.c:93
msgid "Remove all Stations?"
msgstr "¿Quitar todas las emisoras?"

#. We don't do it yet
#: src/ui/gv-station-dialog.c:205
msgid "Security Exception removed"
msgstr "Se quitó la excepción de seguridad"

#: src/ui/gv-station-dialog.c:354
msgid "Save"
msgstr "Guardar"

#: src/ui/gv-station-view.c:188
msgid "kbps"
msgstr "kb/s"

#: src/ui/gv-station-view.c:190
msgid "unknown"
msgstr "desconocido"

#. TRANSLATORS: we talk about nominal bitrate here.
#: src/ui/gv-station-view.c:194 src/ui/gv-station-view.c:204
msgid "nominal"
msgstr "nominal"

#: src/ui/gv-station-view.c:199 src/ui/gv-station-view.c:205
msgid "min"
msgstr "mín."

#: src/ui/gv-station-view.c:200 src/ui/gv-station-view.c:206
msgid "max"
msgstr "máx."

#: src/ui/gv-station-view.c:221
msgid "Mono"
msgstr "Monoaural"

#: src/ui/gv-station-view.c:224
msgid "Stereo"
msgstr "Estéreo"

#: src/ui/gv-station-view.c:270
msgid "kHz"
msgstr "kHz"

#: src/ui/gv-status-icon.c:139
msgid "muted"
msgstr "silenciado"

#: src/ui/gv-status-icon.c:143
msgid "vol."
msgstr "vol."

#: src/ui/gv-status-icon.c:150
msgid "No station"
msgstr "No hay emisora"

#: src/ui/gv-status-icon.c:157
msgid "No metadata"
msgstr "No hay metadatos"

#: src/feat/gv-hotkeys.c:145
msgid "Failed to bind the following keys"
msgstr "No se pudieron asignar las teclas siguientes"

#: src/feat/gv-inhibitor.c:120
msgid "Failed to inhibit system sleep"
msgstr "No se pudo desactivar la suspensión del sistema"

#: src/feat/gv-inhibitor.c:144 src/feat/gv-notifications.c:67
#: src/feat/gv-notifications.c:121
msgid "Playing"
msgstr "Reproduciendo"

#: src/feat/gv-notifications.c:61
#, c-format
msgid "Playing %s"
msgstr "Reproduciendo %s"

#: src/feat/gv-notifications.c:64
#, c-format
msgid "Playing <%s>"
msgstr "Reproduciendo <%s>"

#: src/feat/gv-notifications.c:107
msgid "(Unknown title)"
msgstr "(Título desconocido)"

#~ msgid "Downloading playlist…"
#~ msgstr "Descargando la lista de reproducción…"

#~ msgid "Connecting…"
#~ msgstr "Conectando…"

#~ msgid "Buffering…"
#~ msgstr "Almacenando en búfer…"

#~ msgid "Failed to download playlist"
#~ msgstr "Error al descargar la lista de reproducción"

#~ msgid "Failed to parse playlist"
#~ msgstr "Error al analizar la lista de reproducción"

#~ msgid "Station URL"
#~ msgstr "Dirección url de la Emisora"

#~ msgid "URI"
#~ msgstr "URI"

#, c-format
#~ msgid "%s: %s"
#~ msgstr "%s: %s"

#, c-format
#~ msgid "'%s' is neither a known station or a valid URI"
#~ msgstr "«%s» no es ni una emisora conocida ni un URI válido"

#, c-format
#~ msgid "An error happened while trying to play %s."
#~ msgstr "Se produjo un error al intentar reproducir %s."

#~ msgid "Add"
#~ msgstr "Añadir"

#, c-format
#~ msgid ""
#~ "%s:\n"
#~ "%s"
#~ msgstr ""
#~ "%s:\n"
#~ "%s"

#~ msgid "Autoset Window Height"
#~ msgstr "Establecer automáticamente altura de ventana"

#~ msgid ""
#~ "Automatically adjust the window height when a station is added or removed."
#~ msgstr ""
#~ "Ajustar automáticamente el tamaño de la ventana cuando se añade o quita "
#~ "una emisora."

#~ msgid "stopped"
#~ msgstr "detenido"

#~ msgid "connecting"
#~ msgstr "conectando"

#~ msgid "buffering"
#~ msgstr "almacenando en búfer"

#~ msgid "playing"
#~ msgstr "reproduciendo"

#~ msgid "unknown state"
#~ msgstr "estado desconocido"

#~ msgid "io.gitlab.Goodvibes"
#~ msgstr "io.gitlab.Goodvibes"

#~ msgid "Station Information"
#~ msgstr "Información de la Estación"

#~ msgid "Playing Station"
#~ msgstr "Reproduciendo Estación"

#~ msgid "New Track"
#~ msgstr "Nueva Pista"

#~ msgid "Misc"
#~ msgstr "Varios"

#~ msgid "Menu"
#~ msgstr "Menú"

#~ msgid "Help"
#~ msgstr "Ayuda"

#~ msgid "Toggle Play/Pause"
#~ msgstr "Alternar Reproducir/Pausar"
