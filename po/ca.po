# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the goodvibes package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: goodvibes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-28 16:52+0700\n"
"PO-Revision-Date: 2022-09-24 18:21+0000\n"
"Last-Translator: Maite Guix <maite.guix@gmail.com>\n"
"Language-Team: Catalan <https://hosted.weblate.org/projects/goodvibes/"
"translations/ca/>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.14.1\n"

#: data/io.gitlab.Goodvibes.appdata.xml.in:4
#: data/io.gitlab.Goodvibes.desktop.in:3 src/main.c:129
msgid "Goodvibes"
msgstr "Goodvibes"

#: data/io.gitlab.Goodvibes.appdata.xml.in:5
#: data/io.gitlab.Goodvibes.desktop.in:5
msgid "Play web radios"
msgstr "Reproduïu ràdios web"

#: data/io.gitlab.Goodvibes.appdata.xml.in:8
msgid "Arnaud Rebillout"
msgstr "Arnaud Rebillout"

#: data/io.gitlab.Goodvibes.appdata.xml.in:12
msgid "Goodvibes is a simple internet radio player for GNU/Linux."
msgstr ""
"El Goodvibes és un reproductor de ràdio per internet simple per al GNU/Linux."

#: data/io.gitlab.Goodvibes.appdata.xml.in:15
msgid ""
"It comes with every basic features you can expect from an audio player, such "
"as multimedia keys, notifications, system sleep inhibition, and MPRIS2 "
"support."
msgstr ""
"Inclou totes les funcionalitats esperables d’un reproductor d’àudio, com ara "
"compatibilitat amb les tecles multimèdia i el MPRIS2, notificacions i "
"supressió de l’estat de repòs del sistema."

#: data/io.gitlab.Goodvibes.desktop.in:4
msgid "Radio Player"
msgstr "Reproductor de ràdio"

#. TRANSLATORS: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/io.gitlab.Goodvibes.desktop.in:7
msgid "Audio;Radio;Player;"
msgstr "Àudio;Ràdio;Reproductor;"

#: src/ui/resources/app-menu.ui:6 src/ui/resources/prefs-window.glade:563
#: src/ui/resources/shortcuts-window.ui:16
#: src/ui/resources/status-icon-menu.ui:6
msgid "Play/Stop"
msgstr "Reprodueix/atura"

#: src/ui/resources/app-menu.ui:10 src/ui/resources/shortcuts-window.ui:30
#: src/ui/resources/status-icon-menu.ui:10 src/ui/gv-station-context-menu.c:34
#: src/ui/gv-station-dialog.c:500
msgid "Add Station"
msgstr "Afegeix una emissora"

#: src/ui/resources/app-menu.ui:16 src/ui/resources/status-icon-menu.ui:16
#: src/ui/gv-prefs-window.c:599
msgid "Preferences"
msgstr "Preferències"

#: src/ui/resources/app-menu.ui:22
msgid "Keyboard Shortcuts"
msgstr "Dreceres de teclat"

#: src/ui/resources/app-menu.ui:26 src/ui/resources/shortcuts-window.ui:37
#: src/ui/resources/status-icon-menu.ui:22
msgid "Online Help"
msgstr "Ajuda en línia"

#: src/ui/resources/app-menu.ui:30 src/ui/resources/status-icon-menu.ui:26
msgid "About"
msgstr "Quant a"

#: src/ui/resources/app-menu.ui:34 src/ui/resources/prefs-window.glade:42
#: src/ui/resources/shortcuts-window.ui:44
msgid "Close"
msgstr "Tanca"

#: src/ui/resources/app-menu.ui:38 src/ui/resources/prefs-window.glade:41
#: src/ui/resources/shortcuts-window.ui:51
#: src/ui/resources/status-icon-menu.ui:30
msgid "Quit"
msgstr "Surt"

#: src/ui/resources/playlist-view.glade:49
#: src/ui/resources/station-view.glade:25 src/ui/gv-playlist-view.c:109
#: src/ui/gv-station-view.c:362
msgid "No station selected"
msgstr "No s’ha seleccionat cap emissora"

#: src/ui/resources/playlist-view.glade:65
#: src/ui/resources/station-view.glade:41
msgid "Stopped"
msgstr "Aturat"

#: src/ui/resources/prefs-window.glade:55
msgid "Close Button"
msgstr "Botó per a tancar"

#: src/ui/resources/prefs-window.glade:68
msgid "Application"
msgstr "Aplicació"

#: src/ui/resources/prefs-window.glade:91
msgid "Autoplay on Startup"
msgstr "Reprodueix automàticament en iniciar"

#: src/ui/resources/prefs-window.glade:104
msgid "Custom Output Pipeline"
msgstr "Canonada de sortida personalitzada"

#: src/ui/resources/prefs-window.glade:135
msgid "Apply"
msgstr "Aplica"

#: src/ui/resources/prefs-window.glade:158
msgid "Playback"
msgstr "Reproducció"

#: src/ui/resources/prefs-window.glade:195
msgid "Prevent sleep while playing"
msgstr "Impideix el repòs mentre es reprodueixi la ràdio"

#: src/ui/resources/prefs-window.glade:208
msgid "System"
msgstr "Sistema"

#: src/ui/resources/prefs-window.glade:255
msgid "Native D-Bus Server"
msgstr "Servidor natiu del D-Bus"

#: src/ui/resources/prefs-window.glade:267
msgid "MPRIS2 D-Bus Server"
msgstr "Servidor MPRIS2 del D-Bus"

#: src/ui/resources/prefs-window.glade:280
msgid "D-Bus"
msgstr "D-Bus"

#: src/ui/resources/prefs-window.glade:296
#: src/ui/resources/shortcuts-window.ui:11
msgid "General"
msgstr "General"

#: src/ui/resources/prefs-window.glade:324
msgid "Theme Variant"
msgstr "Variant del tema"

#: src/ui/resources/prefs-window.glade:337
msgid "System Default"
msgstr "Per defecte del sistema"

#: src/ui/resources/prefs-window.glade:338
msgid "Prefer Dark"
msgstr "Prefereix la clara"

#: src/ui/resources/prefs-window.glade:339
msgid "Prefer Light"
msgstr "Prefereix la fosca"

#: src/ui/resources/prefs-window.glade:353
msgid "Window"
msgstr "Finestra"

#: src/ui/resources/prefs-window.glade:380
msgid "Send Notifications"
msgstr "Envia notificacions"

#: src/ui/resources/prefs-window.glade:403
msgid "Notifications"
msgstr "Notificacions"

#: src/ui/resources/prefs-window.glade:439
msgid "Console Output"
msgstr "Sortida de la consola"

#: src/ui/resources/prefs-window.glade:452
msgid "Console"
msgstr "Consola"

#: src/ui/resources/prefs-window.glade:471
msgid "Display"
msgstr "Visualització"

#: src/ui/resources/prefs-window.glade:510
msgid "Multimedia Hotkeys"
msgstr "Tecles multimèdia"

#: src/ui/resources/prefs-window.glade:523
msgid "Keyboard"
msgstr "Teclat"

#: src/ui/resources/prefs-window.glade:550
msgid "Middle Click"
msgstr "Clic mig"

#: src/ui/resources/prefs-window.glade:564
#: src/ui/resources/shortcuts-window.ui:23
msgid "Mute"
msgstr "Silencia"

#: src/ui/resources/prefs-window.glade:577
msgid "Scrolling"
msgstr "Desplaçament"

#: src/ui/resources/prefs-window.glade:590
msgid "Change Station"
msgstr "Canvia d’emissora"

#: src/ui/resources/prefs-window.glade:591
msgid "Change Volume"
msgstr "Canvia el volum"

#: src/ui/resources/prefs-window.glade:605
msgid "Mouse (Status Icon Mode)"
msgstr "Ratolí (mode d’icona d’estat)"

#: src/ui/resources/prefs-window.glade:624
msgid "Controls"
msgstr "Controls"

#: src/ui/resources/station-dialog.glade:13
msgid "Name"
msgstr "Nom"

#: src/ui/resources/station-dialog.glade:36
#: src/ui/resources/station-view.glade:447
msgid "URL"
msgstr "URL"

#: src/ui/resources/station-dialog.glade:63
msgid "Security Exception"
msgstr "Excepció de seguretat"

#: src/ui/resources/station-dialog.glade:74 src/ui/gv-station-context-menu.c:96
msgid "Remove"
msgstr "Suprimeix"

#: src/ui/resources/station-view.glade:89
msgid "Station"
msgstr "Emissora"

#: src/ui/resources/station-view.glade:103
msgid "Comment"
msgstr "Comentari"

#: src/ui/resources/station-view.glade:114
msgid "Year"
msgstr "Any"

#: src/ui/resources/station-view.glade:125
msgid "Genre"
msgstr "Gènere"

#: src/ui/resources/station-view.glade:136
msgid "Album"
msgstr "Àlbum"

#: src/ui/resources/station-view.glade:147
msgid "Artist"
msgstr "Artista"

#: src/ui/resources/station-view.glade:158
msgid "Title"
msgstr "Títol"

#: src/ui/resources/station-view.glade:247
msgid "Metadata"
msgstr "Metadades"

#: src/ui/resources/station-view.glade:261 src/feat/gv-notifications.c:139
msgid "Error"
msgstr "Error"

#: src/ui/resources/station-view.glade:275
msgid "Message"
msgstr ""

#: src/ui/resources/station-view.glade:314
msgid "Details"
msgstr "Detalls"

#: src/ui/resources/station-view.glade:325
msgid "Bitrate"
msgstr "Taxa de bits"

#: src/ui/resources/station-view.glade:336
msgid "Sample Rate"
msgstr "Freqüència de mostratge"

#: src/ui/resources/station-view.glade:347
msgid "Channels"
msgstr "Canals"

#: src/ui/resources/station-view.glade:358
msgid "Codec"
msgstr "Còdec"

#: src/ui/resources/station-view.glade:369
msgid "Stream Type"
msgstr "Tipus de flux"

#: src/ui/resources/station-view.glade:380
msgid "User-Agent"
msgstr "Agent d’usuari"

#: src/ui/resources/station-view.glade:391
#: src/ui/resources/station-view.glade:425 src/ui/gv-certificate-dialog.c:336
#: src/ui/gv-certificate-dialog.c:338
msgid "Redirection"
msgstr ""

#: src/ui/resources/station-view.glade:402 src/ui/gv-certificate-dialog.c:337
#, fuzzy
msgid "Stream URL"
msgstr "Fluxos"

#: src/ui/resources/station-view.glade:414
msgid "Streams"
msgstr "Fluxos"

#: src/ui/resources/station-view.glade:436 src/ui/gv-certificate-dialog.c:335
msgid "Playlist URL"
msgstr ""

#: src/core/gv-engine.c:259
msgid "Failed to parse pipeline description"
msgstr "No s’ha pogut analitzar la descripció de la canonada"

#: src/core/gv-station-list.c:1316
msgid "Failed to save station list"
msgstr "No s’ha pogut desar la llista d’emissores"

#: src/ui/gv-certificate-dialog.c:339
#, fuzzy
msgid "TLS Errors"
msgstr "Error"

#: src/ui/gv-certificate-dialog.c:355
#, fuzzy
msgid "Add a Security Exception?"
msgstr "Voleu afegir una excepció de seguretat?"

#: src/ui/gv-certificate-dialog.c:356
msgid ""
"The TLS certificate for this station is not valid. The issue is most likely "
"a misconfiguration of the website."
msgstr ""

#: src/ui/gv-certificate-dialog.c:363 src/ui/gv-station-context-menu.c:95
#: src/ui/gv-station-dialog.c:353
msgid "Cancel"
msgstr "Cancel·la"

#: src/ui/gv-certificate-dialog.c:364
msgid "Continue"
msgstr ""

#: src/ui/gv-prefs-window.c:259
msgid "Feature disabled at compile-time."
msgstr "S’ha inhabilitat la funció durant la compilació."

#: src/ui/gv-prefs-window.c:401
msgid "Action when the close button is clicked."
msgstr "Acció a fer quan es fa clic al botó Tanca."

#: src/ui/gv-prefs-window.c:407
msgid "Setting not available in status icon mode."
msgstr "El paràmetre no està disponible si utilitzeu el mode d’icona d’estat."

#: src/ui/gv-prefs-window.c:411
msgid "Whether to start playback automatically on startup."
msgstr "Si s’ha d’iniciar la reproducció automàticament en engegar."

#: src/ui/gv-prefs-window.c:417
msgid "Whether to use a custom output pipeline."
msgstr "Si s’ha d’utilitzar una canonada de sortida personalitzada."

#: src/ui/gv-prefs-window.c:423
msgid ""
"The GStreamer output pipeline used for playback. Refer to the online "
"documentation for examples."
msgstr ""
"La canonada de sortida del GStreamer que s’utilitza per a la reproducció. "
"Llegiu la documentació en línia per a obtenir exemples."

#: src/ui/gv-prefs-window.c:439
msgid "Prevent the system from going to sleep while playing."
msgstr "Evita que el sistema entri en repòs durant la reproducció."

#: src/ui/gv-prefs-window.c:444
msgid "Enable the native D-Bus server (needed for the command-line interface)."
msgstr ""
"Habilita el servidor natiu del D-Bus (necessari per a la interfície de línia "
"d’ordres)."

#: src/ui/gv-prefs-window.c:450
msgid "Enable the MPRIS2 D-Bus server."
msgstr "Habilita el servidor MPRIS2 del D-Bus."

#. Display
#: src/ui/gv-prefs-window.c:456
msgid "Prefer a different variant of the theme (if available)."
msgstr "Prefereix una variant diferent del tema (si n’hi ha)."

#: src/ui/gv-prefs-window.c:462
msgid "Show notification when the status changes."
msgstr "Mostra una notificació quan canviï l’estat."

#: src/ui/gv-prefs-window.c:467
msgid "Display information on the standard output."
msgstr "Mostra informació a la sortida estàndard."

#. Controls
#: src/ui/gv-prefs-window.c:473
msgid "Bind mutimedia keys (play/pause/stop/previous/next)."
msgstr ""
"Vincula les tecles multimèdia (reprodueix/posa en pausa/atura/anterior/"
"següent)."

#: src/ui/gv-prefs-window.c:479
msgid "Action triggered by a middle click on the status icon."
msgstr "Acció activada en fer un clic mig a la icona d’estat."

#: src/ui/gv-prefs-window.c:485
msgid "Action triggered by mouse-scrolling on the status icon."
msgstr "Acció activada en desplaçar-se amb el ratolí sobre la icona d’estat."

#: src/ui/gv-prefs-window.c:491
msgid "Setting only available in status icon mode."
msgstr ""
"El paràmetre només està disponible si utilitzeu el mode d’icona d’estat."

#. We might be creating a new station, or editing an existing one
#: src/ui/gv-station-context-menu.c:35 src/ui/gv-station-dialog.c:500
msgid "Edit Station"
msgstr "Edita l’emissora"

#: src/ui/gv-station-context-menu.c:36
msgid "Remove Station"
msgstr "Suprimeix l’emissora"

#: src/ui/gv-station-context-menu.c:37
msgid "Remove all Stations"
msgstr "Suprimir totes les emissores"

#: src/ui/gv-station-context-menu.c:93
msgid "Remove all Stations?"
msgstr "Suprimir totes les emissores?"

#. We don't do it yet
#: src/ui/gv-station-dialog.c:205
msgid "Security Exception removed"
msgstr "S’ha suprimit l’excepció de seguretat"

#: src/ui/gv-station-dialog.c:354
msgid "Save"
msgstr "Desa"

#: src/ui/gv-station-view.c:188
msgid "kbps"
msgstr "kb/s"

#: src/ui/gv-station-view.c:190
msgid "unknown"
msgstr "desconeguda"

#. TRANSLATORS: we talk about nominal bitrate here.
#: src/ui/gv-station-view.c:194 src/ui/gv-station-view.c:204
msgid "nominal"
msgstr "nominal"

#: src/ui/gv-station-view.c:199 src/ui/gv-station-view.c:205
msgid "min"
msgstr "mínima"

#: src/ui/gv-station-view.c:200 src/ui/gv-station-view.c:206
msgid "max"
msgstr "màxima"

#: src/ui/gv-station-view.c:221
msgid "Mono"
msgstr "Monofònic"

#: src/ui/gv-station-view.c:224
msgid "Stereo"
msgstr "Estereofònic"

#: src/ui/gv-station-view.c:270
msgid "kHz"
msgstr "kHz"

#: src/ui/gv-status-icon.c:139
msgid "muted"
msgstr "silenciat"

#: src/ui/gv-status-icon.c:143
msgid "vol."
msgstr "vol."

#: src/ui/gv-status-icon.c:150
msgid "No station"
msgstr "Cap emissora"

#: src/ui/gv-status-icon.c:157
msgid "No metadata"
msgstr "Cap metadada"

#: src/feat/gv-hotkeys.c:145
msgid "Failed to bind the following keys"
msgstr "No s’han pogut vincular les tecles següents"

#: src/feat/gv-inhibitor.c:120
msgid "Failed to inhibit system sleep"
msgstr "No s’ha pogut impedir el repòs del sistema"

#: src/feat/gv-inhibitor.c:144 src/feat/gv-notifications.c:67
#: src/feat/gv-notifications.c:121
msgid "Playing"
msgstr "S’està reproduint"

#: src/feat/gv-notifications.c:61
#, c-format
msgid "Playing %s"
msgstr "S’està reproduint %s"

#: src/feat/gv-notifications.c:64
#, c-format
msgid "Playing <%s>"
msgstr "S’està reproduint <%s>"

#: src/feat/gv-notifications.c:107
msgid "(Unknown title)"
msgstr "(Títol desconegut)"

#~ msgid "Connecting…"
#~ msgstr "S’està connectant…"

#~ msgid "Buffering…"
#~ msgstr "S’està omplint la memòria intermèdia…"

#, fuzzy
#~ msgid "Failed to download playlist"
#~ msgstr "No s’ha pogut desar la llista d’emissores"

#, fuzzy
#~ msgid "Failed to parse playlist"
#~ msgstr "No s’ha pogut desar la llista d’emissores"

#, fuzzy
#~ msgid "Station URL"
#~ msgstr "Emissora"

#~ msgid "URI"
#~ msgstr "URI"

#, c-format
#~ msgid "%s: %s"
#~ msgstr "%s: %s"

#, c-format
#~ msgid "'%s' is neither a known station or a valid URI"
#~ msgstr "«%s» no és una emissora coneguda ni un URI vàlid"

#, c-format
#~ msgid "An error happened while trying to play %s."
#~ msgstr "S’ha produït un error en intentar reproduir %s."

#~ msgid "Add"
#~ msgstr "Afegeix"

#, c-format
#~ msgid ""
#~ "%s:\n"
#~ "%s"
#~ msgstr ""
#~ "%s:\n"
#~ "%s"

#~ msgid "Autoset Window Height"
#~ msgstr "Defineix automàticament la mida de la finestra"

#~ msgid ""
#~ "Automatically adjust the window height when a station is added or removed."
#~ msgstr ""
#~ "Ajusta automàticament l’alçada de la finestra en afegir o suprimir una "
#~ "emissora."

#~ msgid "stopped"
#~ msgstr "aturat"

#~ msgid "connecting"
#~ msgstr "s’està connectant"

#~ msgid "buffering"
#~ msgstr "s’està omplint la memòria intermèdia"

#~ msgid "playing"
#~ msgstr "s’està reproduint"

#~ msgid "unknown state"
#~ msgstr "estat desconegut"

#~ msgid "io.gitlab.Goodvibes"
#~ msgstr "io.gitlab.Goodvibes"
